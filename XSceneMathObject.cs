﻿using UnityEngine;

using static XUtils;

[ExecuteAlways]
public class XSceneMathObject : MonoBehaviour
{
    //Methods
    //-Child API
    //--Initialization
    protected virtual void initializeOnConstruction() { }
    protected virtual void reinitializeAfterScriptChange() { }
    protected virtual void commmonInitialization() {
        _updater = new XCustomUpdater(
            internal_update,
            () => {
                return isValid(this);
            },
            (FastArray<XCustomUpdater> outUpdaters) => {
                if (!isReadyForUse()) return;

                _register_dependencies.clear(false);
                collectDependencies(_register_dependencies);
                foreach (XSceneMathObject theDependenciesEditorObjects in _register_dependencies) {
                    outUpdaters.add(verify(theDependenciesEditorObjects)._updater);
                }
            }
        );
    }

    //--Updating
    virtual protected bool isReadyForUse() { return true; }

    protected virtual void editorUpdate() { }
    protected virtual void ingameUpdate() { }
    protected virtual void updateVisualizers() { }

    //--Dependencies
    //TODO: Make internal
    protected virtual void collectDependencies(FastArray<XSceneMathObject> outDependencies) { }

    //-Implementation
    private void Awake() { OnConstruct(); }

    [UnityEditor.Callbacks.DidReloadScripts]
    static private void OnScriptsReloaded() {
        XSceneMathObject[] theEditorObjects = FindObjectsOfType<XSceneMathObject>();
        foreach (XSceneMathObject theEditorObject in theEditorObjects) {
            theEditorObject.OnRefreshAfterScriptChange();
        }
    }

    private void OnConstruct() {
        initializeOnConstruction();
        commmonInitialization();
    }

    private void OnRefreshAfterScriptChange() {
        reinitializeAfterScriptChange();
        commmonInitialization();
    }

    internal void internal_update() {
        if (!isReadyForUse()) return;

        if (Application.IsPlaying(gameObject)) {
            ingameUpdate();
        } else {
            editorUpdate();
        }

        updateVisualizers();
    }

    private XCustomUpdater _updater = null;

    //TODO: Use here "object register pattern" for sharing this memory for all Editor Objects
    private FastArray<XSceneMathObject> _register_dependencies = new FastArray<XSceneMathObject>();
}

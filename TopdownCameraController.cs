﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

#if UNITY_EDITOR
[ExecuteAlways]
#endif
public class TopdownCameraController : MonoBehaviour
{
#if false

    //Methods
    //-Implementation
    private void Awake() {
        _camera = XUtils.getComponent<Camera>(
            this, XUtils.AccessPolicy.ShouldExist
        );

        Vector3 theRotation = _camera.transform.eulerAngles;
        theRotation.x = 90.0f;
        _camera.transform.eulerAngles = theRotation;

        XUtils.check(_objectToFocus);
    }

    private void FixedUpdate() {
        updateCameraTransform();
    }

#if UNITY_EDITOR
    private void Update() {
        if (Application.IsPlaying(gameObject)) return;
        updateCameraTransform();
    }
#endif

    private void updateCameraTransform() {
        float theHeightInUnits = XMetrics.distanceToUnits(_height);

        if (_fullAttach) {
            _camera.transform.eulerAngles = _objectToFocus.transform.eulerAngles;
            _camera.transform.Rotate(90.0f, _cameraRotation, 0.0f);

            _camera.transform.position = _objectToFocus.transform.TransformPoint(
                new Vector3(0.0f, theHeightInUnits, 0.0f)
            );
        } else {
            Vector3 thePosition = _objectToFocus.transform.position;
            thePosition.y += theHeightInUnits;
            _camera.transform.position = thePosition;
        }
    }

    //Fields
    private Camera _camera = null;

    //-Settings
    [SerializeField] private GameObject _objectToFocus = null;
    [SerializeField] private float _height = 100.0f;

    [SerializeField] internal bool _fullAttach = false;
    [SerializeField, HideInInspector] internal float _cameraRotation = 0.0f;
}

[CustomEditor(typeof(TopdownCameraController))]
public class MyScriptEditor : Editor
{
    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        var theScript = (TopdownCameraController)target;
        if (theScript._fullAttach) {
            theScript._cameraRotation = EditorGUILayout.FloatField(
                "Camera rotation", theScript._cameraRotation
            );
        }
    }

#endif
}

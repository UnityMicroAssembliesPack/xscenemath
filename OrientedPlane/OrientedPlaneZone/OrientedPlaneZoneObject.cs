﻿using UnityEngine;

[ExecuteAlways]
public class OrientedPlaneZoneObject : XSceneMathObject
{
    //Methods
    //-API
    public void setZoneVerticies(Vector2[] inVertices) {
        _vertices = inVertices;
    }

    public XMathTypes.OrientedPlane getPlane() {
        return getPlaneObject().getPlane();
    }

    //-Implementation
    private void Start() {
        _zoneVisualizer.setVisibilityInHierarchy(false);
    }

    protected override sealed bool isReadyForUse() {
        if (!base.isReadyForUse()) return false;
        if (!XUtils.isValid(_zoneVisualizer)) return false;
        if (!XUtils.isValid(getPlaneObject())) return false;
        return true;
    }

    protected override sealed void updateVisualizers() {
        base.updateVisualizers();

        XMathTypes.LocatorTransform thePlaneTransform = getPlaneObject().getPlane().transform;
        _zoneVisualizer.set(_vertices, thePlaneTransform.position, thePlaneTransform.axisY, thePlaneTransform.axisX);
    }

    //-Utils
    private OrientedPlaneObject getPlaneObject() {
        return transform.parent?.GetComponent<OrientedPlaneObject>();
    }

    //Fields
    [SerializeField, PrefabModeOnly] private ConvexPolygonObject _zoneVisualizer = null;

    [SerializeField] private Vector2[] _vertices = new Vector2[] {
        new Vector2(-1.0f , -1.0f),
        new Vector2(-1.0f ,  1.0f),
        new Vector2( 1.0f ,  1.0f),
        new Vector2( 1.0f , -1.0f),
    };
}

﻿using UnityEngine;
using XMathTypes;

public class OrientedPlaneObject : XSceneMathObject
{
    //Methods
    //-API
    public OrientedPlane getPlane() {
        return new OrientedPlane(transform.position, transform.up, transform.right);
    }

    //-Implementation
    private void Start() {
        _planeTransformVisualizer.setVisibilityInHierarchy(false);
        _planeQuadVisualizer.setVisibilityInHierarchy(false);
    }

    protected sealed override bool isReadyForUse() {
        if (!base.isReadyForUse()) return false;

        if (!XUtils.isValid(_planeTransformVisualizer)) return false;
        if (!XUtils.isValid(_planeQuadVisualizer)) return false;
        return true;
    }

    protected sealed override void updateVisualizers() {
        base.updateVisualizers();

        LocatorTransform theTransform = getPlane().transform;

        _planeTransformVisualizer.set(theTransform);
        _planeTransformVisualizer.setVisibilityVisual(_showVisualizers);

        _planeQuadVisualizer.set(theTransform.position, theTransform.axisY, kPlaneQuadVisualizerSize);
        _planeQuadVisualizer.setVisible(_showVisualizers);
    }

    //Fields
    //-Visualizers
    private static Vector2 kPlaneQuadVisualizerSize = new Vector2(0.4f, 0.4f);

    [SerializeField] private bool _showVisualizers = false;
    [SerializeField, PrefabModeOnly] private BasisObject _planeTransformVisualizer = null;
    [SerializeField, PrefabModeOnly] private QuadObject _planeQuadVisualizer = null;
}

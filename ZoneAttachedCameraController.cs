﻿using UnityEngine;

//using CameraTypes;
//using BoxedCameraSettings = Box<CameraTypes.CameraSettings>;

//TODO: Correct negative positions (down the plane)?
// (placing camera in zero point)

#if UNITY_EDITOR
[ExecuteAlways]
#endif
public class ZoneAttachedCameraController : MonoBehaviour
{
#if false

    //Methods
    //-Implementation
    private void Awake() {
        _camera = XUtils.getComponent<CameraSettingsHolder>(
            this, XUtils.AccessPolicy.ShouldExist
        ).getSettings();

        XUtils.check(_zone);

        //TODO: Correct initial camera position

        //TODO: Support updating of camera position in editor
    }

    private void FixedUpdate() {
        updateCameraCorrection();
    }

    private void updateCameraCorrection() {
        //if (_camera.Equals(_lastStateData.camera)) return;
        //
        //Ray theViewRay = _camera.value.getViewRay();
        //Vector2 thePotentialPlaneLookPoint = _zone.getPlaneLookPoint(theViewRay);
        //
        ////TODO: Correct negative positions (looking to side oposite then Zone)?
        //// In any way we may get rotation correction needing based on diraction
        //// difference of Camera Offset
        //
        //Vector3 theOffset = _camera.value.position -
        //    _zone.getWorldPoint(thePotentialPlaneLookPoint);
        //setPlaneLookPoint(thePotentialPlaneLookPoint, theOffset);
    }

    private void setPlaneLookPoint(Vector2 inPlaneLookPoint, Vector3 inCameraOffset) {
        XUtils.check(!XMath.isInfinityVector(inPlaneLookPoint));

        Vector2 theResultPlaneLookPoint = inPlaneLookPoint;

        if (!_zone.isPlaneLookPointInZone(inPlaneLookPoint)) {
            theResultPlaneLookPoint = _zone.getFirstZoneIntersectionForPlaneSegment(
                new XMath.Segment2D(_lastStateData.planeLookPoint, inPlaneLookPoint)
            ).value;

            _camera.value.position = _zone.getWorldPoint(theResultPlaneLookPoint) + inCameraOffset;
        }

        _lastStateData.camera = _camera.value;
        _lastStateData.planeLookPoint = theResultPlaneLookPoint;
    }

#if UNITY_EDITOR
    private void Update() {
        if (!Application.isEditor) return;

        correctInitialCameraInEditor();
    }

    private void correctInitialCameraInEditor() {

        //TODO: Find nearest point in Zone from current not corrected look point
        // (if point is is zone - this operation should return point itself)
        
        //TODO: Correct looking to oposite diraction

        //TODO: Calculate initial offset for not corrected (initial) camera position
        
        //TODO: Set _lastStateData & _camera based on offset
        // (using specific comutation, not setPlaneLookPoint)
    }
#endif

    //Fields
    private BoxedCameraSettings _camera = null;

    //-Settings
    [SerializeField] private PlaneZoneObject _zone = null;

    //-Runtime
    struct LastStateData {
        public CameraSettings camera;
        public Vector2 planeLookPoint;
    }
    LastStateData _lastStateData = new LastStateData();


#endif
}

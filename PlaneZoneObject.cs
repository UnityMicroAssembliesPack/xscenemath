﻿using UnityEngine;

#if UNITY_EDITOR
[ExecuteAlways]
#endif
public class PlaneZoneObject : MonoBehaviour
{
    //Methods
    //-API
    //TODO: Return infinity only for XZ params & correct for Y
    public bool isPlaneLookPointInZone(Vector2 inLookPoint) {
        //return _zone.isPointInside(inLookPoint);

        return false;
    }

    public Vector2 getPlanePointForRay(Ray inRay) {
        bool UNUSED = false;
        return getPlaneLookPoint(inRay, out UNUSED);
    }

    public Vector2 getPlaneLookPoint(Ray inRay, out bool outIsNotInZone) {
        //Plane thePlane = XMath.getTransformPlane(transform, XMath.TransformPlane.XZ);
        //
        //float theDistance;
        //bool theIsInifinity = !thePlane.Raycast(inRay, out theDistance);
        //Vector2 thePlaneLookPoint = getPlanePoint(inRay.GetPoint(theDistance));
        //
        //outIsNotInZone = theIsInifinity || theDistance < 0.0f ||
        //    !isPlaneLookPointInZone(thePlaneLookPoint);
        //
        //return theIsInifinity ? XMath.getInifinityVector2() :
        //    thePlaneLookPoint;

        outIsNotInZone = false;
        return new Vector2();
    }

    //public Optional<Vector2> getFirstZoneIntersectionForPlaneSegment(
    //    XMath.Segment2D inSegment)
    //{
    //    return _zone.getFirstSegmentIntersection(inSegment);
    //}

//public Optional<Vector3> getFirstZoneIntersectionForSegment(
//    XMath.Segment3D inSegment, bool inCheckIsOnPlane = false)
//{
//    var theSegment = new XMath.Segment2D(
//        getPlanePoint(inSegment.pointA, inCheckIsOnPlane),
//        getPlanePoint(inSegment.pointB, inCheckIsOnPlane)
//    );
//    Optional<Vector2> theInstersection =
//        getFirstZoneIntersectionForPlaneSegment(theSegment);
//
//    return theInstersection.isSet() ?
//        new Optional<Vector3>(getWorldPoint(theInstersection.value)) :
//        new Optional<Vector3>();
//}
//

//public Vector2 getPlanePoint(Vector3 inWorldPoint, bool inCheckIsOnPlane = false) {
//    if (XMath.isInfinityVector(inWorldPoint)) {
//        if (inCheckIsOnPlane) {
//            XUtils.check(false);
//        }
//        return XMath.getInifinityVector2();
//    }
//
//    Vector3 theLocalPoint = transform.InverseTransformPoint(inWorldPoint);
//    theLocalPoint.y = 0.0f;
//
//    if (inCheckIsOnPlane) {
//        const float kPrecision = 0.001f;
//        Vector3 thePointOnPlane = transform.TransformPoint(theLocalPoint);
//        float theDistance = (inWorldPoint - thePointOnPlane).magnitude;
//        XUtils.check(XMath.equalsWithPrecision(theDistance, 0.0f, kPrecision));
//    }
//
//    return XMath.getXZFromVector(theLocalPoint);
//}
//

#if false

    public Vector3 getWorldPoint(Vector2 inPlanePoint) {
        return XMath.isInfinityVector(inPlanePoint) ? XMath.getInifinityVector3() :
            transform.TransformPoint(XMath.getXZVector(inPlanePoint));
    }

    //-Implementation
    private void Awake() {
        initZoneFromSettings();
    }

    private void initZoneFromSettings() {
        _zone = new XMath.ConvexPolygon2D(_zoneVertices);
    }

#if UNITY_EDITOR
    //TODO: Move debug draw to separate place (another component)
    private void OnValidate() {
        initZoneFromSettings();
    }

    private void OnDrawGizmos() {
        Mesh theGizmosMesh = getGizmosMesh();
        if (!theGizmosMesh) return;

        XDebug.drawMesh(theGizmosMesh, transform);

        Vector3 theCenter = transform.TransformPoint(
            XMath.getXZVector(_zone.getCenter())
        );
        XDebug.drawArrow(theCenter, theCenter + transform.up);
    }

    private Mesh getGizmosMesh() {
        if (!isNeedToRebuildGizmosMesh()) {
            return _debug_gizmosMesh;
        }

        if (_debug_gizmosMesh) {
            DestroyImmediate(_debug_gizmosMesh);
            _debug_gizmosMesh = null;
        }

        if (!_zone.isValid()) {
            return null;
        }

        return _debug_gizmosMesh = XDebug.createMeshForPolygon(_zone);
    }

    private bool isNeedToRebuildGizmosMesh() {
        if (!_debug_gizmosMesh) return true;
        if (!_zone.isValid()) return true;

        Vector2[] theVertices = _zone.getVertices();
        int theVerticesNum = theVertices.Length;
        if (theVerticesNum != _debug_gizmosMesh.vertexCount) {
            return true;
        }

        for (int theIndex = 0; theIndex < theVerticesNum; ++theIndex) {
            Vector2 theVertex = theVertices[theIndex];
            Vector3 theMeshVertex = _debug_gizmosMesh.vertices[theIndex];
            if (theVertex.x != theMeshVertex.x || theVertex.y != theMeshVertex.y) {
                return true;
            }
        }

        return false;
    }

    private Mesh _debug_gizmosMesh = null;
#endif

    //Fields
    //-Settings
    [SerializeField] Vector2[] _zoneVertices = null;

    //-State
    private XMath.ConvexPolygon2D _zone;


#endif
}

﻿using UnityEngine;

[ExecuteAlways]
public class PlaneAttachLocatorObject : LocatorObject
{
    //Methods
    //-API
    public sealed override XMathTypes.LocatorTransform getTransform() {
        return getPlaneObject().getPlane().getTransformFor(_planePoint, _planeYaw);
    }

    public Vector2 planePoint {
        get { return _planePoint; }
        set { _planePoint = value; }
    }
    
    public float planeYaw {
        get { return _planeYaw; }
        set { _planeYaw = value; }
    }

    //-Child API
    protected sealed override bool isReadyForUse() {
        if (!base.isReadyForUse()) return false;

        return XUtils.isValid(getPlaneObject());
    }

    protected sealed override void collectDependencies(FastArray<XSceneMathObject> outDependencies) {
        base.collectDependencies(outDependencies);

        outDependencies.add(getPlaneObject());
    }

    //-Utils
    private OrientedPlaneObject getPlaneObject() {
        return transform.parent?.GetComponent<OrientedPlaneObject>();
    }

    //Fields
    //-Settings
    [SerializeField] private Vector2 _planePoint;
    [SerializeField] private float _planeYaw;
}

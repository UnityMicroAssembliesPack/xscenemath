﻿using UnityEngine;
using XMathTypes;

[ExecuteAlways]
public class LocatorAttachLocatorObject : LocatorObject
{
    //Methods
    //-API
    public override sealed LocatorTransform getTransform() {
        return new LocatorTransform(getPointOnDiraction(_offset), Quaternion.identity);
    }

    public float offset {
        get { return _offset; }
        set { _offset = value; }
    }

    public Vector3 offsetAngles {
        get { return _offsetAngle; }
        set { _offsetAngle = value; }
    }

    //-Child API
    protected sealed override bool isReadyForUse() {
        if (!base.isReadyForUse()) return false;

        if (!XUtils.isValid(getLocatorToAttach())) return false;
        if (!XUtils.isValid(_offsetDiractionVisualizer)) return false;

        return true;
    }

    protected sealed override void updateVisualizers() {
        base.updateVisualizers();

        LocatorTransform theTransformToAttach = getLocatorToAttach().getTransform();
        _offsetDiractionVisualizer.set(theTransformToAttach.position, getPointOnDiraction(1.0f));

        _offsetDiractionVisualizer.setVisibilityVisual(isVisualizersVisible());
        _offsetDiractionVisualizer.setVisibilityInHierarchy(XUtils.isPrefabContext(this));
    }

    //-Utils
    private Vector3 getDiractionVector() {
        LocatorTransform theTransformToAttach = getLocatorToAttach().getTransform();
        return (theTransformToAttach.rotation * Quaternion.Euler(_offsetAngle)) * Vector3.right;
    }

    private Vector3 getPointOnDiraction(float inOffset) {
        LocatorTransform theTransformToAttach = getLocatorToAttach().getTransform();
        return theTransformToAttach.position + getDiractionVector() * inOffset;
    }

    private float getOffsetForWorldPointProjectedToDiraction(Vector3 inPoint) {
        LocatorTransform theTransformToAttach = getLocatorToAttach().getTransform();
        Vector3 thePositionDiraction = theTransformToAttach.position - transform.position;

        return Vector3.Project(thePositionDiraction, getDiractionVector()).magnitude;
    }

    private LocatorObject getLocatorToAttach() {
        //TODO: Return null if Locator is not ready for use to prevent Exceptions on Update
        return transform.parent?.GetComponent<LocatorObject>();
    }

    //Fields
    //-Settings
    [SerializeField] private float _offset = 0;
    [SerializeField] private Vector3 _offsetAngle = new Vector3(0.0f, 0.0f, 0.0f);

    //-Editor state
    [SerializeField, PrefabModeOnly] private ArrowObject _offsetDiractionVisualizer = null;
}

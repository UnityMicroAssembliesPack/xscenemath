﻿using UnityEngine;
using XMathTypes;

public abstract class LocatorObject : XSceneMathObject
{
    //Methods
    //-Public
    public abstract LocatorTransform getTransform();

    //-Child API
    protected void setFreeTransform() {
        _isTransformFree = true;
    }

    protected override bool isReadyForUse() {
        if (!base.isReadyForUse()) return false;

        return XUtils.isValid(_locatorTransformVisualizer);
    }

    protected override void editorUpdate() {
        base.editorUpdate();

        if (!_isTransformFree) {
            getTransform().applyForTransform(transform);
            transform.hideFlags = transform.hideFlags | HideFlags.HideInInspector;
        } else {
            transform.hideFlags = transform.hideFlags & ~HideFlags.HideInInspector;
        }
    }

    protected override void ingameUpdate() {
        base.ingameUpdate();

        getTransform().applyForTransform(transform);
    }

    protected bool isVisualizersVisible() {
        return _showVisualizers;
    }

    protected override void updateVisualizers() {
        base.updateVisualizers();

        _locatorTransformVisualizer.set(getTransform());
        _locatorTransformVisualizer.setVisibilityVisual(_showVisualizers);
        _locatorTransformVisualizer.setVisibilityInHierarchy(XUtils.isPrefabContext(this));
    }

    //Fields
    [SerializeField, PrefabModeOnly] private BasisObject _locatorTransformVisualizer = null;
    [SerializeField] private bool _showVisualizers = false;

    [SerializeField, HideInInspector] private bool _isTransformFree = false;
}

﻿using System;
using UnityEngine;
using XMathTypes;

//TODO: Reorganized with external updating logic (like Limited Rotated Locator is implemented)
public class TravellingLocatorObject : LocatorObject
{
    //-Public
    public override sealed LocatorTransform getTransform() {
        return XMath.getGlobalTransform(_targetLocator.getTransform(), _currentLocatorTransformRelatedToTarget);
    }

    public void setTargetLocator(LocatorObject inLocator, Action<TravellingLocatorObject> inOnAchievedAction = null) {
        XUtils.check(inLocator);
        if (inLocator == _targetLocator) return;

        LocatorTransform theCurrentTransform = getTransform();
        _targetLocator = inLocator;
        _currentLocatorTransformRelatedToTarget = XMath.getLocalTransform(_targetLocator.getTransform(), theCurrentTransform);

        _onAchievedAction = inOnAchievedAction;
    }

    public bool isTransiting {
        get { return (1.0f != getTransitionProgress()); }
    }

    //-Child API
    protected override bool isReadyForUse() {
        if (!base.isReadyForUse()) return false;

        if (!XUtils.isValid(_targetLocator)) return false;

        return true;
    }

    //--Updating
    protected override void editorUpdate() {
        _currentLocatorTransformRelatedToTarget = new LocatorTransform(new Vector3());
        base.editorUpdate();
    }

    protected override void ingameUpdate() {
        base.ingameUpdate();

        float theTransitionProgress = getTransitionProgress();

        if (theTransitionProgress >= 1.0f) {
            _currentLocatorTransformRelatedToTarget = LocatorTransform.identity;

            if (XUtils.isValid(_onAchievedAction)) {
                _onAchievedAction.Invoke(this);
                _onAchievedAction = null;
            }
        } else {
            _currentLocatorTransformRelatedToTarget = LocatorTransform.lerp(
                _currentLocatorTransformRelatedToTarget, LocatorTransform.identity, theTransitionProgress
            );
        }
    }

    protected float getTransitionProgress() {
        float theDistance = _currentLocatorTransformRelatedToTarget.position.magnitude;
        if (0.0f == theDistance) return 1.0f;

        float theDeltaTime = Time.deltaTime;
        float theDistancePerDelta = _speed * theDeltaTime;

        return (theDistancePerDelta / theDistance);
    }

    //Fields
    //-Settings
    [SerializeField] private LocatorObject _targetLocator = null;
    [SerializeField] private float _speed = 1.0f;

    //-Runtime
    [SerializeField, HideInInspector]
    private LocatorTransform _currentLocatorTransformRelatedToTarget = LocatorTransform.identity;

    //TODO: Replace with Unity Serializable Event
    private Action<TravellingLocatorObject> _onAchievedAction = null;
}

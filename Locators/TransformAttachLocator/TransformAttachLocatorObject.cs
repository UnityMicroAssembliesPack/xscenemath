﻿using UnityEngine;
using XMathTypes;

public class TransformAttachLocatorObject : LocatorObject
{
    //Methods
    //-API
    public override LocatorTransform getTransform() {
        LocatorTransform theLocalTransform = new LocatorTransform(_position, Quaternion.Euler(_rotation));
        LocatorTransform theParentTransform = new LocatorTransform(transform.parent);
        return theParentTransform.getGlobalTransform(theLocalTransform);
    }

    public Vector3 position {
        get { return _position; }
        set { _position = value; }
    }

    public Vector3 rotation {
        get { return _rotation; }
        set { _rotation = value; }
    }

    //Fields
    [SerializeField] Vector3 _position = Vector3.zero;
    [SerializeField] Vector3 _rotation = Vector3.zero;
}

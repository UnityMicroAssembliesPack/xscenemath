﻿using UnityEngine;
using XMathTypes;

public class LimitedRotatedLocatorObject : LocatorObject
{
    //Methods
    //-Public
    public override sealed LocatorTransform getTransform() {
        LocatorTransform theLocalTransform = new LocatorTransform(_position, Quaternion.Euler(0.0f, _yaw, _pitch));
        LocatorTransform theParentTransform = new LocatorTransform(transform.parent);
        return theParentTransform.getGlobalTransform(theLocalTransform);
    }

    public float pitch {
        get { return _pitch; }
    }
    public float setPitch(float inWantedNewPitch) {
        return _pitch = getValueForLimits(inWantedNewPitch, _pitchLimitFrom, _pitchLimitTo);
    }

    public float yaw {
        get { return _yaw; }
    }
    public float setYaw(float inWantedNewYaw) {
        return _yaw = getValueForLimits(inWantedNewYaw, _yawLimitFrom, _yawLimitTo);
    }

    public void getAnglesDeltaForTargetDiraction(out float outYawDelta, out float outPitchDelta,
        float inYawMaxDelta, float inPitchMaxDelta,
        Vector3 inTargetDiraction, Locality inDiractionLocality = Locality.Global)
    {
        LocatorTransform theParentTransform = new LocatorTransform(transform.parent);

        Vector3 theDiractionLocal = new Vector3();
        switch (inDiractionLocality) {
            case Locality.Local:  theDiractionLocal = inTargetDiraction; break;
            case Locality.Global: theDiractionLocal = theParentTransform.getLocalDiraction(inTargetDiraction); break;
            default:              XUtils.check(false); break;
        }

        float theTargetYaw, theTargetPitch;
        XMath.getPitchAndYawFromDiraction(out theTargetYaw, out theTargetPitch, theDiractionLocal);

        outYawDelta = getValueUpdateDelta(_yaw, theTargetYaw, _yawLimitFrom, _yawLimitTo, inYawMaxDelta);
        outPitchDelta = getValueUpdateDelta(_pitch, theTargetPitch, _pitchLimitFrom, _pitchLimitTo, inPitchMaxDelta);
    }

    //-Implementation
    protected override sealed void editorUpdate() {
        _yaw = getValueForLimits(_yaw, _yawLimitFrom, _yawLimitTo);
        _pitch = getValueForLimits(_pitch, _pitchLimitFrom, _pitchLimitTo);

        base.editorUpdate();
    }

    //--Utils
    static float getValueUpdateDelta(float inValue, float inTargetValue,
        float inLimitFrom, float inLimitTo,
        float inMaxPossibleDelta)
    {
        float theTargetValueWithLimits = getValueForLimits(inTargetValue, inLimitFrom, inLimitTo);
        if (inValue == theTargetValueWithLimits) return 0.0f;

        float theDelta = XMath.getNormalizedAngle(theTargetValueWithLimits - inValue);
        return (Mathf.Abs(theDelta) < inMaxPossibleDelta) ? theDelta :
            inMaxPossibleDelta * Mathf.Sign(theDelta);
    }

    static float getValueForLimits(float inValue, float inLimitFrom, float inLimitTo) {
        inValue = XMath.getNormalizedAngle(inValue);
        if (inLimitFrom == inLimitTo) return inLimitFrom;

        float theLimitsRange = getNonperiodicAngleRange(inLimitFrom, inLimitTo);
        float theTargetValueRange = getNonperiodicAngleRange(inLimitFrom, inValue);
        if (theTargetValueRange <= theLimitsRange) return inValue;

        float theInverseLimitsRange = getNonperiodicAngleRange(inLimitTo, inLimitFrom);
        float theInverseTargetValueRange = getNonperiodicAngleRange(inLimitTo, inValue);
        return (theInverseTargetValueRange < theInverseLimitsRange/2) ? inLimitTo : inLimitFrom;
    }

    static float getNonperiodicAngleRange(float inStartingAngle, float inAngleToCalculateDistanceTo) {
        return inStartingAngle < inAngleToCalculateDistanceTo ?
            inAngleToCalculateDistanceTo - inStartingAngle :
            (360.0f - XMath.getNonperiodicAngle(inStartingAngle)) +
                    XMath.getNonperiodicAngle(inAngleToCalculateDistanceTo);
    }

    //Fields
    [SerializeField] private Vector3 _position = Vector3.zero;

    [SerializeField] private float _yaw = 0.0f;
    [SerializeField] private float _yawLimitFrom = -180.0f;
    [SerializeField] private float _yawLimitTo = 180.0f;

    [SerializeField] private float _pitch = 0.0f;
    [SerializeField] private float _pitchLimitFrom = -180.0f;
    [SerializeField] private float _pitchLimitTo = 180.0f;
}

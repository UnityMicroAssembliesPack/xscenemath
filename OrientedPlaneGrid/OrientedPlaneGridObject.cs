﻿using UnityEngine;

public class OrientedPlaneGridObject : XSceneMathObject
{
    //Methods
    //-API

    //-Implementation
    protected override sealed bool isReadyForUse() {
        if (!base.isReadyForUse()) return false;
        if (!XUtils.isValid(getPlaneObject())) return false;
        if (!XUtils.isValid(_gridVisualizer)) return false;
        return true;
    }

    protected override sealed void updateVisualizers() {
        XMathTypes.LocatorTransform thePlaneTransform = getPlaneObject().getPlane().transform;
        _gridVisualizer.set(thePlaneTransform.position, thePlaneTransform.axisY, thePlaneTransform.axisX,
            _xCellsNum, _yCellsNum, _gridSize, _gridSize,
            0.01f
        );
    }

    //-Utils
    OrientedPlaneObject getPlaneObject() {
        return transform.parent?.GetComponent<OrientedPlaneObject>();
    }

    //Fields
    [SerializeField] private GridObject _gridVisualizer = null;

    [SerializeField] private float _gridSize = 1.0f;
    [SerializeField] private Vector2 _offset = new Vector2();

    [SerializeField] private int _xCellsNum = 1;
    [SerializeField] private int _yCellsNum = 1;
}
